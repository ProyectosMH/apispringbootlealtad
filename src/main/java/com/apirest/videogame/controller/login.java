package com.apirest.videogame.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apirest.videogame.model.User;
import com.apirest.videogame.repository.UserService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/api/videogame")
public class login {
	@Autowired
    UserService userService;
	@Value("${jwt.secret}")
	private String SECRET;
	
	@PostMapping("/login")
	public User login(@RequestHeader("username") String username, @RequestHeader("password") String password) {
		User user = new User();
		user = userService.authenticate(username, password);
		if(user != null) {
			String token = getJWTToken(user.getUsername());
			user.setToken(token);
		}else {
			user = new User();
			user.setUsername("Usuario o contraseña incorrecta");
		}
				
		return user;
		
	}
	
	private String getJWTToken(String username) {
		String secretKey = SECRET; //"mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
}
